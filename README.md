﻿   ##                                     ***Beer Tag***


Beer Tag web application enables users to manage all the beers that they have drank and want to drink. Each beer has detailed information about it. Data is community driven and every beer lover can add new beers. 

***
* Visibility without authentication
   * the application start page
   * browse all beers and their details
   * sort/filter functionality for beers
   *  user login page 
   *  user register form

 * When authenticated, user can:
   *  create new beers
   *  edit already existing beers
   * see his wish/drunk list
   * add a beer to his wish/drunk list
   *  rate a beer after the beer was added to user's drunk list
   * see and modify their personal information 

### ***Routes***
|Mapping | Description|
|--------|:------------|
|/ 	| Home page|
|/about|About page|
|/menu|	Beer page with all the beers and sort/filter functionality|
|/beer/{beer_id}| A beer's detail page|
|/beers/new|Create a new beer|
|/edit-beer/{beer_id}|Edit beer information|
|/sort/name|Beer page with all beers sorted by name|
|/sort/abv|Beer page with all beers sorted by ABV(alcohol by volume)|
|/filter/country|Page where users can filter beers by origin country|
|/filter/style|Page where users can filter beers by style|
|/login|Login page providing authentication via username and password|
|/register|Registration page|
|/profile| User's profile page|
|/edit-user/{username}| Edit user's information|
|

### ***Screenshots***
- Home page without authentication

![home_page](/images/home_page.PNG)

- Home page with authentication

![home_page](/images/home_page2.PNG)

- Registration page

![register_page](/images/register_page.PNG)

- Login page

![login_page](/images/login_page.PNG)

- User's profile page

![user_profile_page](/images/profile_details.PNG)

- Edit user's information

![edit_user_page](/images/edit_account_page.PNG)

- Beer page 

![beer_page](/images/beerpage1.PNG)

![beer_page](/images/beer_page2.PNG)


- Beer profile page with authentication

![beer_profile_page](/images/beer_details_whenauthorized.PNG)

- Create a beer page

![create_beer_page](/images/createbeer_page.PNG)

- Edit a beer page

![edit_beer_page](/images/editbeer_page.PNG)





























