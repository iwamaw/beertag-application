package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;

public interface RatingService {
    void create(User user, Beer beer, int rating);
}
