package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> getAll();

    Brewery getOne(int id);

    List<Brewery> getByName(String name);

    void create(Brewery brewery);

    void delete(int id);
}
