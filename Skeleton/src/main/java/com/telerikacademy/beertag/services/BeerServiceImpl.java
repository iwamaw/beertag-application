package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.services.common.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.services.common.TagService;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.Beer;
import org.springframework.stereotype.Service;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class BeerServiceImpl implements BeerService {

    private BeerRepository beerRepository;
    private TagService tagService;

    @Autowired
    public BeerServiceImpl(BeerRepository repository, TagService tagService) {
        this.beerRepository = repository;
        this.tagService = tagService;
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public Beer getOne(int id) {
        List<Beer> beers = beerRepository.getOne(id);
        if (beers.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_BEER_BY_ID, id));
        }
        return beers.get(0);
    }

    public List<Beer> getByName(String name) {
        return beerRepository.getByName(name);
    }

    @Override
    public void create(Beer beer) {
        if (beerRepository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXISTS, beer.getName())
            );
        }
        beerRepository.create(beer);
    }

    @Override
    public void delete(int id) {

        Beer beer = getOne(id);
        beerRepository.delete(beer); //exception to catch in controller
    }

    @Override
    public void update(Beer beer) {

        if (!beerRepository.checkBeerExists(beer.getName())) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_BEER_BY_NAME, beer.getName())
            );
        }
        beerRepository.update(beer);
    }

    public List<Beer> filterByStyle(String name) {
        return beerRepository.filterByStyle(name);
    }

    public List<Beer> filterByTag(int tag_id) {

        return beerRepository.filterByTag(tag_id);
    }

    public List<Beer> filterByCountry(String name) {
        return beerRepository.filterByCountry(name);
    }

    public List<Beer> sortByABV() {
        return beerRepository.sortByABV();
    }

    public List<Beer> sortByName() {
        return beerRepository.sortByName();
    }

    public List<Beer> sortByRating() {
        return beerRepository.sortByRating();
    }

    public void addTagToBeer(int beer_id, int tag_id) {
        Beer beer = getOne(beer_id);
        Tag tag = tagService.getOne(tag_id);
        if (beerRepository.checkIfTagIsAlreadyInList(beer_id, tag_id)) {
            throw new DuplicateEntityException(
                    String.format(TAG_ALREADY_EXIST_IN_LIST_OF_TAGS, tag_id, beer_id)
            );
        }
        beer.addTag(tag);
        beerRepository.update(beer);
    }

    public List<Rating> getAllRatingForBeer(int beer_id) {
        return beerRepository.getAllRatingForBeer(beer_id);
    }

}
