package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.UsersRepository;
import com.telerikacademy.beertag.services.common.RatingService;
import com.telerikacademy.beertag.services.common.BeerService;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;

import java.util.List;
import java.util.Set;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private RatingService ratingService;
    private UsersRepository repository;
    private BeerService beerService;

    @Autowired
    public UserServiceImpl(RatingService ratingService,
                           UsersRepository repository,
                           BeerService beerService) {
        this.ratingService = ratingService;
        this.beerService = beerService;
        this.repository = repository;
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    public User getOne(int id) {
        List<User> users = repository.getOne(id);
        if (users.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_USER_BY_ID,id));
        }
        return users.get(0);
    }

    @Override
    public User getByUsername(String username) {

        List<User> users = repository.getByUsername(username);
        if (users.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_USER_BY_USERNAME,username)); //TODO excpetions
        }
        return users.get(0);
    }

    @Override
    public void update(User user) {
        if (!repository.checkUserExists(user.getUsername())) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_USER_BY_USERNAME, user.getUsername())
            );
        }
        repository.update(user);
    }

    @Override
    public void create(User user) {
        if (repository.checkUserExists(user.getUsername())) {
            throw new DuplicateEntityException(
                    String.format(USER_ALREADY_EXISTS, user.getUsername())
            );
        }
        repository.create(user);
    }

    @Override
    public void delete(int id) {
        User user = getOne(id);
        repository.delete(user);
    }


    public void addToList(String listOfChoice, int user_id, int beer_id) {
        User user = getOne(user_id);
        Beer beer = beerService.getOne(beer_id);

        if (listOfChoice.equalsIgnoreCase("wishlist")) {
            addToWishList(user, beer);

        } else if (listOfChoice.equalsIgnoreCase("dranklist")) {

            addToDrankList(user, beer);
        } else {

            throw new IllegalArgumentException(String.format(NOT_EXISTING_LIST, listOfChoice));
        }
    }

    public void removeFromList(String list, int user_id, int beer_id) {
        User user = getOne(user_id);
        Beer beer = beerService.getOne(beer_id);
        repository.removeFromList(list, user, beer);
    }

    public Set<Beer> showList(String listOfChoice, int user_id) {
        User user = getOne(user_id);
        return repository.showList(listOfChoice, user);
    }

    public void rateBeer(int user_id, int beer_id, int rating) {

        if (repository.checkIfBeerIsRated(user_id, beer_id)) {
            repository.removeRateForBeer(user_id, beer_id);
        }

        User user=getOne(user_id);
        Beer beer=beerService.getOne(beer_id);

        ratingService.create(user, beer, rating);
    }

    private void addToWishList(User user, Beer beer) {

        if (repository.checkIfBeerExistInWishList(user.getId(), beer.getID())) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXIST_IN_WISHLIST, beer.getID(), user.getId()));
        }
        user.addToWishlist(beer);
        repository.update(user);
    }

    private void addToDrankList(User user, Beer beer) {

        if (repository.checkIfBeerExistInDrankList(user.getId(), beer.getID())) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXIST_IN_DRANKLIST, beer.getID(), user.getId()));
        }
        user.addToDranklist(beer);
        repository.update(user);
    }

}
