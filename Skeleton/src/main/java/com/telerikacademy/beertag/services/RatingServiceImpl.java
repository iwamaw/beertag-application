package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.repository.common.RatingRepository;
import com.telerikacademy.beertag.services.common.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository ratingRepository;


    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;

    }

    public void create(User user, Beer beer, int rating) {
        ratingRepository.create(user, beer, rating);
    }
}
