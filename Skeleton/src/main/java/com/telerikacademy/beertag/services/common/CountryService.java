package com.telerikacademy.beertag.services.common;


import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();

    Country getOne(int id);

    List<Country> getByName(String name);

    void create(Country country);

    void delete(int id);
}
