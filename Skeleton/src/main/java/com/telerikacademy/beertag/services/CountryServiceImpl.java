package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.repository.common.CountryRepository;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.services.common.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.models.Country;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public Country getOne(int id) {
        List<Country> countries = repository.getOne(id);
        if (countries.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_COUNTRY_BY_ID, id));
        }
        return countries.get(0);
    }

    public List<Country> getByName(String name) {
        return repository.getByName(name);
    }

    public void create(Country country) {
        if (repository.checkCountryExists(country.getName())) {
            throw new DuplicateEntityException(
                    String.format(COUNTRY_ALREADY_EXISTS, country.getName()));
        }
        repository.create(country);
    }

    @Override
    public void delete(int id) {
        Country country = getOne(id);
        repository.delete(country);
    }

}
