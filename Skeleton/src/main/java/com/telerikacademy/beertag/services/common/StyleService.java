package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    Style getOne(int id);

    List<Style> getByName(String nam);

    void create(Style style);

    void delete(int id);
}
