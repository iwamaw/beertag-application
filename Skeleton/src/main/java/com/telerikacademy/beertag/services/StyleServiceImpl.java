package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.StyleRepository;
import com.telerikacademy.beertag.services.common.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.models.Style;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class StyleServiceImpl implements StyleService {

    private StyleRepository repository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }

    @Override
    public Style getOne(int id) {
        List<Style> styles = repository.getOne(id);
        if (styles.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_STYLE_BY_ID, id));
        }
        return styles.get(0);
    }

    public List<Style> getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public void create(Style style) {
        if (repository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(
                    String.format(STYLE_ALREADY_EXISTS, style.getName()));
        }
        repository.create(style);
    }

    @Override
    public void delete(int id) {
        Style style = getOne(id);
        repository.delete(style);
    }
}
