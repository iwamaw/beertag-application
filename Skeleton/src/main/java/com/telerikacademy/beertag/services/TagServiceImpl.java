package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.TagRepository;
import com.telerikacademy.beertag.services.common.TagService;
import org.springframework.stereotype.Service;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository repository;

    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    public List<Tag> getAll() {
        return repository.getAll();
    }

    public Tag getOne(int id) {
        List<Tag> tags = repository.getOne(id);
        if (tags.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_TAG_BY_ID, id));
        }
        return tags.get(0);
    }

    public List<Tag> getByName(String name) {
        return repository.getByName(name);
    }

    public void create(Tag tag) {
        if (repository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(
                    String.format(TAG_ALREADY_EXISTS, tag.getName()));
        }
        repository.create(tag);
    }

    public void delete(int id) {
        Tag tag = getOne(id);
        repository.delete(tag);
    }


}
