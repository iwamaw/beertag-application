package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagService {

    List<Tag> getAll();

    Tag getOne(int id);

    List<Tag> getByName(String name);

    void create(Tag tag);

    void delete(int id);
}
