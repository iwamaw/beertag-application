package com.telerikacademy.beertag.services;

public class ServiceConstants {

     //Constants for user class
     static final String NOT_FOUND_USER_BY_ID="User with id:%d does not exist.";
     static final String USER_ALREADY_EXISTS="User with name %s already exists.";
     static final String NOT_FOUND_USER_BY_USERNAME="User with username:%s does not exist.";

     //Constants for beer class
     static final String NOT_FOUND_BEER_BY_ID ="Beer with id:%d does not exist.";
     static final String BEER_ALREADY_EXISTS="Beer with name:%s already exists.";
     static final String NOT_FOUND_BEER_BY_NAME="Beer with name: %s already exists.";
     static final String BEER_ALREADY_EXIST_IN_WISHLIST="Beer with id:%d already exist in wishlist of user with id:%d.";
     static final String BEER_ALREADY_EXIST_IN_DRANKLIST="Beer with id:%d already exist in dranklist of user with id:%d.";

     //Constants for brewery class
     static final String BREWERY_ALREADY_EXIST="Brewery with name:%s already exists.";
     static final String NOT_FOUND_BREWERY_BY_ID="Brewery with id:%d does not exists.";

     //Constants for country class
     static final String NOT_FOUND_COUNTRY_BY_ID="Country with id:%d does not exist.";
     static final String COUNTRY_ALREADY_EXISTS= "Country with name:%s already exists.";

     //Constants for style class
     static final String NOT_FOUND_STYLE_BY_ID="Style with id:%d does not exist.";
     static final String STYLE_ALREADY_EXISTS="Style with name:%s already exists.";

     //Constants for tag class
     static final String NOT_FOUND_TAG_BY_ID="Tag with id:%d does not exist.";
     static final String TAG_ALREADY_EXISTS="Tag with name:%s already exists.";


     //
     static final String NOT_EXISTING_LIST="%s does not exist for this user.";
     static final String TAG_ALREADY_EXIST_IN_LIST_OF_TAGS="Tag with id:%d already exist in list of beer with id:%d.";
}
