package com.telerikacademy.beertag.services;

import org.springframework.stereotype.Service;
import com.telerikacademy.beertag.models.Brewery;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.services.common.BreweryService;
import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.BreweryRepository;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class BreweryServiceImpl implements BreweryService {

    private BreweryRepository repository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAll();
    }

    @Override
    public Brewery getOne(int id) {
        List<Brewery> breweries = repository.getOne(id);
        if (breweries.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_BREWERY_BY_ID, id));
        }
        return breweries.get(0);
    }

    public List<Brewery> getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public void create(Brewery brewery) {
        if (repository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(
                    String.format(BREWERY_ALREADY_EXIST, brewery.getName()));
        }
        repository.create(brewery);
    }

    @Override
    public void delete(int id) {
        Brewery brewery = getOne(id);
        repository.delete(brewery);
    }
}
