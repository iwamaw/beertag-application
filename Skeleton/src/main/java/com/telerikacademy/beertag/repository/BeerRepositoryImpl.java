package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Beer", Beer.class).list();
        }
    }

    @Override
    public List<Beer> getOne(int id) {
        try (Session session = sessionFactory.openSession()) {

            Query<Beer> query = session.createQuery(
                    "from Beer where id= :id", Beer.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    public List<Beer> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name LIKE :name", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void delete(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(beer);
            session.getTransaction().commit();
        }
    }


    public void update(Beer newBeer) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(newBeer);
            session.getTransaction().commit();
        }
    }


    public List<Beer> filterByStyle(String name) {
        try (Session session = sessionFactory.openSession()) {
            List<Style> styles = getStyleIfExist(name);
            if (styles.size() != 0) {
                int styleID = styles.get(0).getID();
                Query<Beer> beerQuery = session.createQuery(
                        "from Beer where style.id=:styleID", Beer.class
                );
                beerQuery.setParameter("styleID", styleID);
                return new ArrayList<>(beerQuery.list());

            }
            return new ArrayList<>();
        }
    }

    public List<Beer> filterByCountry(String name) { //int country_id
        try (Session session = sessionFactory.openSession()) {

            List<Country> countries = getCountryIfExist(name);

            if (countries.size() != 0) {

                List<Brewery> breweries = getBreweriesByCountry(countries.get(0).getID());

                List<Beer> filteredBeers = new ArrayList<>();

                for (Brewery brewery : breweries) {
                    int breweryID = brewery.getID();
                    Query<Beer> beerQuery = session.createQuery(
                            "from Beer where brewery.id=:breweryID", Beer.class
                    );
                    beerQuery.setParameter("breweryID", breweryID);
                    filteredBeers.addAll(beerQuery.list());
                }
                return filteredBeers;
            }
            return new ArrayList<>();
        }
    }

    public List<Beer> filterByTag(int tag_id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = getTagIfExist(tag_id);

            session.beginTransaction();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> beerCriteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beerRoot = beerCriteriaQuery.from(Beer.class);
            beerCriteriaQuery.select(beerRoot);
            Predicate predicate = criteriaBuilder.equal(beerRoot.get("tags"), "#beer");
            beerCriteriaQuery.where(predicate);
            Query<Beer> beerQuery = session.createQuery(beerCriteriaQuery);
            List<Beer> allBeers = beerQuery.getResultList();

            List<Beer> result = new ArrayList<>();

            for (Beer beer : allBeers) {
                if (beer.getTags().contains(tag)) {
                    result.add(beer);
                }
            }
            session.getTransaction().commit();
            return result;
        }
    }


    public List<Beer> sortByABV() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(
                    "from Beer order by abv asc", Beer.class).list();
        }
    }

    public List<Beer> sortByName() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(
                    "from Beer order by name asc", Beer.class).list();
        }
    }

    public List<Beer> sortByRating() {
        try (Session session = sessionFactory.openSession()) {
//            return session.createQuery(
//                    "from Beer order by ratings asc", Beer.class).list();
            return new ArrayList<>();
        }

    }

    //TODO sort by rating

    public boolean checkIfTagIsAlreadyInList(int beer_id, int tag_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery(
                    "select beer_id, tag_id from beers_tags where beer_id=:beer_id and tag_id=:tag_id");
            query.setParameter("beer_id", beer_id);
            query.setParameter("tag_id", tag_id);
            return query.list().size() != 0;
        }
    }

    public boolean checkBeerExists(String name) {
        return getByName(name).size() != 0;
    }

    private List<Style> getStyleIfExist(String name) {

        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name LIKE :name", Style.class);
            query.setParameter("name", "%" + name + "%");

            return query.list();
        }
    }

    private Tag getTagIfExist(int tag_id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, tag_id);

            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id:%d does not exist", tag_id));
            }
            return tag;
        }
    }

    private List<Country> getCountryIfExist(String name) {

        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name LIKE :name", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public List<Rating> getAllRatingForBeer(int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery query = session.createSQLQuery("select beer_id from rating where beer_id=:beer_id");
            query.setParameter("beer_id", beer_id);
            return query.list();
        }
    }

    private List<Brewery> getBreweriesByCountry(int id) {
        try (Session session = sessionFactory.openSession()) {

            Query<Brewery> query = session.createQuery(
                    "from Brewery where country.id=:id", Brewery.class);

            query.setParameter("id", id);
            return query.list();
        }
    }
}
