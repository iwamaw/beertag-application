package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;

import java.util.List;
import java.util.Set;

public interface UsersRepository {

    List<User> getAll();
    void create(User user);
    void update(User user);
    void delete (User user);
    List<User> getOne(int id);
    double ratingSumForBeer(int beer_id);
    boolean checkUserExists(String name);
    List<User> getByUsername(String username);
    void removeRateForBeer(int user_id,int beer_id);
    Set<Beer> showList(String listOfChoice, User user);
    boolean checkIfBeerIsRated(int user_id,int beer_id);
    void removeFromList(String list,User user,Beer beer);
    Object getRateForBeer(int user_id, int beer_id, int rating);
    boolean checkIfBeerExistInWishList(int user_id, int beer_id);
    boolean checkIfBeerExistInDrankList(int user_id, int beer_id);


}
