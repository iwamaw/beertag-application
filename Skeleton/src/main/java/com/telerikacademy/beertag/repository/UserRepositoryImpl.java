package com.telerikacademy.beertag.repository;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import com.telerikacademy.beertag.repository.common.UsersRepository;

import java.util.List;
import java.util.Set;

@Repository
public class UserRepositoryImpl implements UsersRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from User", User.class)
                    .list();
        }
    }

    public List<User> getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where id=:id", User.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<User> getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where username = :username", User.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User newUser) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(newUser);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkUserExists(String name) {
        return getByUsername(name) != null;
    }


    public void removeFromList(String listOfChoice, User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {

            if (listOfChoice.equalsIgnoreCase("wishlist")) {
                removeFromWishList(user, beer);

            } else if (listOfChoice.equalsIgnoreCase("dranklist")) {
                removeFromDrankList(user, beer);
            } else {

                throw new IllegalArgumentException(String.format("%s does not exist for this user", listOfChoice));
            }
        }
    }

    public Set<Beer> showList(String listOfChoice, User user) {

        try (Session session = sessionFactory.openSession()) {
            if (listOfChoice.equalsIgnoreCase("wishlist")) {
                return showWishList(user);
            } else if (listOfChoice.equalsIgnoreCase("dranklist")) {
                return showDrankList(user);
            } else {
                throw new IllegalArgumentException(String.format("'%s' does not exist for users.", listOfChoice));
            }
        }
    }

    public boolean checkIfBeerExistInWishList(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id from wishlist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            return query.list().size() != 0;
        }
    }

    public boolean checkIfBeerExistInDrankList(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id " +
                    "from dranklist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            return query.list().size() != 0;
        }
    }

    public boolean checkIfBeerIsRated(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id,rating " +
                    "from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            return query.list().size() != 0;
        }
    }

    public Object getRateForBeer(int user_id, int beer_id, int rating) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id,rating " +
                    "from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            return query.getSingleResult();
        }
    }

    public void removeRateForBeer(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {

            session.beginTransaction();
            Query query = session.createSQLQuery("delete from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    public double ratingSumForBeer(int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query query = session.createSQLQuery("select avg(rating) from rating b where b.beer_id=:beer_id");
            query.setParameter("beer_id", beer_id);
            session.getTransaction().commit();
            return (Double) query.getSingleResult();
        }
    }

    private Set<Beer> showWishList(User user) {
        return user.getWishList();
    }

    private Set<Beer> showDrankList(User user) {
        return user.getDranklist();
    }

    private void removeFromWishList(User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {

            int user_id = user.getId();
            int beer_id = beer.getID();

            session.beginTransaction();
            user.removeFromWishlist(beer);
            Query query = session.createSQLQuery("delete from wishlist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    private void removeFromDrankList(User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {

            int user_id = user.getId();
            int beer_id = beer.getID();

            session.beginTransaction();
            user.removeFromDranklist(beer);
            Query query = session.createSQLQuery("delete from dranklist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }

    }
}
