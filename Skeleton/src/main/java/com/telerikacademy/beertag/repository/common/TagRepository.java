package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagRepository {

    List<Tag> getAll();

    List<Tag> getOne(int id);

    List<Tag> getByName(String name);

    void create(Tag brewery);

    void delete(Tag tag);

    boolean checkTagExists(String name);

}
