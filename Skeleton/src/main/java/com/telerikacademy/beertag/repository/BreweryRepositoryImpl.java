package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.repository.common.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.telerikacademy.beertag.models.Brewery;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Brewery", Brewery.class)
                    .list();
        }
    }

    @Override
    public List<Brewery> getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery(
                    "from Brewery where id=:id", Brewery.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    public List<Brewery> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name LIKE :name", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }

    @Override
    public void delete(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(brewery);
            session.getTransaction().commit();
        }
    }

    public boolean checkBreweryExists(String name) {
        return getByName(name).size() != 0;
    }
}
