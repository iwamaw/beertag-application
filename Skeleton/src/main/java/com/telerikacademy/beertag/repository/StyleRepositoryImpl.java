package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.repository.common.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.telerikacademy.beertag.models.Style;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.
                    createQuery("from Style", Style.class)
                    .list();
        }
    }

    public List<Style> getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery(
                    "from Style where id=:id", Style.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    public List<Style> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name LIKE :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }


    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
    }

    public void delete(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(style);
            session.getTransaction().commit();
        }
    }

    public boolean checkStyleExists(String name) {
        return getByName(name).size() != 0;
    }

}
