package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.repository.common.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import org.hibernate.SessionFactory;
import org.hibernate.Session;


@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory=sessionFactory;
    }

    @Override
    public void create(User user,Beer beer,int rating) {
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            Rating newRating=new Rating(user,beer,rating);
            session.save(newRating);
            session.getTransaction().commit();
        }
    }

}
