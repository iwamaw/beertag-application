package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repository.common.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.telerikacademy.beertag.models.Tag;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Tag> getAll() {

        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Tag", Tag.class)
                    .list();
        }
    }

    public List<Tag> getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery(
                    "from Tag where id=:id", Tag.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    public List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name LIKE :name", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    public void delete(Tag tag) {

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.delete(tag);
                session.getTransaction().commit();
            }
    }

    public boolean checkTagExists(String name) {
        return getByName(name).size() != 0;
    }

}
