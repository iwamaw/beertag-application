package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    List<Brewery> getAll();
    List<Brewery> getOne(int id);
    void create(Brewery brewery);
    void delete(Brewery brewery);
    List<Brewery> getByName(String name);
    boolean checkBreweryExists(String name);
}
