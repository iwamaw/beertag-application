package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;

public interface RatingRepository {
    void create(User user, Beer beer, int rating);
}
