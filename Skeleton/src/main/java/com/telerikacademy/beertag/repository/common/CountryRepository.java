package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();

    List<Country> getByName(String name);

    List<Country> getOne(int id);

    void create(Country country);

    void delete(Country country);

    boolean checkCountryExists(String name);
}
