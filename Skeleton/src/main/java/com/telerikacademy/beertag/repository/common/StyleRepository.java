package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    List<Style> getOne(int id);

    List<Style> getByName(String name);

    void create(Style style);

    void delete(Style style);

    boolean checkStyleExists(String name);
}
