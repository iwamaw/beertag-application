package com.telerikacademy.beertag.models;

import javax.validation.constraints.*;
import java.io.File;

public class BeerDTO {

    @NotBlank
    @NotNull
    @Size(min = 2, max = 20, message = "Name size should be between 2 and 20 symbols.")
    private String name;

    @NotBlank
    @Size(max = 200, message = "Description size should be less than 200 symbols.")
    private String description;

    @Positive(message = "ABV should be positive")
    private double abv;

    @PositiveOrZero(message = "BreweryID should be positive or zero")
    private int breweryID;

    @PositiveOrZero(message = "StyleID should be positive ot zero")
    private int styleID;

    @PositiveOrZero(message = "TagID should be positive ot zero")
    private int tagID;

    private File picture;

    //TODO list of tags
    public BeerDTO() {
    }

    public BeerDTO(String name,
                   double abv,
                   int styleID,
                   int tagId,
                   File picture,
                   int breweryID,
                   String description) {
        setName(name);
        setDescription(description);
        setAbv(abv);
        setBreweryID(breweryID);
        setStyleID(styleID);
        setTagID(tagId);
        setPicture(picture);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getBreweryID() {
        return breweryID;
    }

    public void setBreweryID(int breweryID) {
        this.breweryID = breweryID;
    }

    public int getStyleID() {
        return styleID;
    }

    public void setStyleID(int styleID) {
        this.styleID = styleID;
    }

    public int getTagID() {
        return tagID;
    }

    public void setTagID(int tagID) {
        this.tagID = tagID;
    }

    public File getPicture() {
        return picture;
    }

    public void setPicture(File picture) {
        this.picture = picture;
    }
}
