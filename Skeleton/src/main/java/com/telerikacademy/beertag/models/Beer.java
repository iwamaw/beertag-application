package com.telerikacademy.beertag.models;

import org.hibernate.annotations.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;


@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int beerID;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @Column(name = "abv")
    private double abv;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beers_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tag;

    @Transient
    private double rating;

    @OneToMany(mappedBy = "beer")
    private Set<Rating> ratings;

    @Column(name = "picture")
    private String beerPicture;

    public Beer() {
    }

    public Beer(String name) {
        setName(name);
    }

    public Beer(String name,
                double abv,
                Style style,
                List<Tag> tag,
                double ratings,
                String picture,
                Brewery brewery,
                String description) {
        setName(name);
        setAbv(abv);
        setStyle(style);
        setTags(tag);
        setRating(ratings);
        setBeerPicture(picture);
        setBrewery(brewery);
        setDescription(description);
    }

    public int getID() {
        return beerID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public Style getStyle() {
        return style;
    }

    public double getAbv() {
        return abv;
    }

    public List<Tag> getTags() {
        return tag;
    }

    public String getBeerPicture() {
        return beerPicture;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public void setBeerPicture(String beerPicture) {
        this.beerPicture = beerPicture;
    }

    public void setTags(List<Tag> tag) {
        this.tag = tag;
    }

    public void addTag(Tag tag) {
        this.tag.add(tag);
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public Set<Rating> getAllRating() {
        return ratings;
    }

    public void addRate(Rating rate) {
        ratings.add(rate);
    }

}
