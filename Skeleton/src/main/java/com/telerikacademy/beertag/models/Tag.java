package com.telerikacademy.beertag.models;

import javax.validation.constraints.Size;
import javax.persistence.*;

@Entity
@Table(name="tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private  int id;

    @Column(name="name")
    @Size(min=2,max=15,message = "Name should be between 2 and 15 symbols")
    private String name;

    public Tag(){}

    public Tag(String name){
        setName(name);
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
