package com.telerikacademy.beertag.models;
import javax.validation.constraints.Size;
import javax.persistence.*;

@Entity
@Table(name="country")
public class Country {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int countryID;
    
    @Column(name="name")
    @Size(min = 2,max = 15,message = "Name should be between 2 and 15 symbols")
    private String name;

    public Country() {
    }

    public Country(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return countryID;
    }

    public void setName(String name) {
        this.name = name;
    }
}
