package com.telerikacademy.beertag.models;


import javax.validation.constraints.*;
import javax.persistence.*;

@Entity
@Table(name = "rating")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="beer_id")
    private Beer beer;


    @Column(name = "rating")
    @Positive
    @Min(value = 0,message = "You cannot rate with less than 0 stars")
    @Max(value = 5,message = "You cannot rate with more than 5 stars")
    private int rate;


    public Rating() {
    }

    public Rating(User user, Beer beer, int rate)
    {
        setRate(rate);
        setBeer(beer);
        setUser(user);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
