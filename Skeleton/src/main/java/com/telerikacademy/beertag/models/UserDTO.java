package com.telerikacademy.beertag.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.File;

public class UserDTO {

    @Size(min = 1, message = "Username is required")
    private String username;

    @Size(min = 1, message = "Password is required")
    private String password;

    @Size(min = 1, message = "Verify is required")
    private String passwordConfirmation;

    @Size(min = 1, message = "First name is required")
    private String firstName;

    @Size(min = 1, message = "Last name is required")
    private String lastName;

//    @Min(value = 18, message = "Age should not be less than 18")
//    private int age;

    @Size(min = 1, message = "E-mail is required")
    private String email;

    private File picture;

    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public File getPicture() {
        return picture;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserDTO(File picture) {
        this.picture = picture;
    }
}
