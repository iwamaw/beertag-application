package com.telerikacademy.beertag.models;

import org.hibernate.annotations.FilterDef;

import javax.validation.constraints.Size;
import javax.persistence.*;

@Entity
@Table(name = "style")
//@FilterDef(name = "filerByStyle",defaultCondition = "id= :style_id")
public class Style {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int styleID;

    @Column(name = "name")
    @Size(min=2,max=15,message = "Name should be between 2 and 15 symbols")
    private String name;

    public Style() {
    }

    public Style(String name) {
        this.name = name;
    }

    public int getID() {
        return styleID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
