package com.telerikacademy.beertag.models;

import javax.validation.constraints.Size;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Size(min = 4, max = 11, message = "Username has to be between 4 and 11 characters!")
    @Column(name = "username")
    private String username;

    @Size(min = 1, message = "Password is required!")
    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "profile_pic")
    private String profilePicUrl;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usersroles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "wishlist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> wishList;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "dranklist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> dranklist;

    public User() {
    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Beer> getWishList() {
        return wishList;
    }

    public void setWishList(Set<Beer> wishList) {
        this.wishList = wishList;
    }

    public Set<Beer> getDranklist() {
        return dranklist;
    }

    public void setDranklist(Set<Beer> dranklist) {
        this.dranklist = dranklist;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public void addToWishlist(Beer beer){
        wishList.add(beer);
    }

    public void addToDranklist(Beer beer){
        dranklist.add(beer);
    }

    public void removeFromWishlist(Beer beer){
        wishList.remove(beer);
    }

    public void removeFromDranklist(Beer beer){
        dranklist.remove(beer);
    }

}
