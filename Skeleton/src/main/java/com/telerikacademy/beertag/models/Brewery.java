package com.telerikacademy.beertag.models;

import javax.validation.constraints.Size;
import javax.persistence.*;

@Entity
@Table(name = "brewery")
public class Brewery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int breweryID;

    @Column(name="name")
    @Size(min=2,max=15,message = "Name should be between 2 and 15 symbols")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    public Brewery() {
    }

    public Brewery(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return breweryID;
    }

    public void setName(String name) {
        this.name = name;
    }

}
