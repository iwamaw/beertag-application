package com.telerikacademy.beertag.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import com.telerikacademy.beertag.services.common.BeerService;
import org.springframework.security.core.Authentication;
//import org.graalvm.compiler.lir.LIRInstruction;
import com.telerikacademy.beertag.models.Beer;
import org.springframework.http.HttpStatus;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import com.telerikacademy.beertag.models.User;
import org.springframework.ui.Model;

import java.security.Principal;
import java.util.ArrayList;

@Controller
public class HomeController {
    private UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }

    @GetMapping("/about")
    public String showAboutPage() { return "about";}

    @GetMapping("/profile")
    public String showInformation(Model model,
                                  Principal principal){
        User user = userService.getByUsername(principal.getName());

        model.addAttribute("show-information", user.getUsername());
        model.addAttribute("user",user);
        return "user-profile-page";
    }

}
