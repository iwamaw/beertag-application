package com.telerikacademy.beertag.controller;

import com.telerikacademy.beertag.services.common.BeerService;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import com.telerikacademy.beertag.models.User;
import org.springframework.ui.Model;

import java.security.Principal;

@Controller
public class UserController {

    private UserService userService;
    private BeerService beerService;

    @Autowired
    public UserController(UserService userService,
                          BeerService beerService) {
        this.userService = userService;
        this.beerService = beerService;
    }


    @GetMapping("/wishlist")
    public String showWishlist(Model model,
                               Principal principal) {
        User user = userService.getByUsername(principal.getName());

        model.addAttribute("beers",
                userService.showList("wishlist", user.getId()));
        return "show-wishlist";
    }

    @GetMapping("dranklist")
    public String showDrankList(Model model,
                                Principal principal) {
        User user = userService.getByUsername(principal.getName());

        model.addAttribute("beers",
                userService.showList("dranklist", user.getId()));
        return "show-dranklist";
    }

    @RequestMapping("/wishlist/remove/{beer_id}")
    public String removeBeerFromWishlist(Model model,
                                         Principal principal,
                                         @PathVariable int beer_id) {
        User user = userService.getByUsername(principal.getName());
        userService.removeFromList("wishlist", user.getId(), beer_id);

        model.addAttribute("beers",
                userService.showList("wishlist", user.getId()));
        return "show-wishlist";
    }

    @RequestMapping("/dranklist/remove/{beer_id}")
    public String removeBeerFromDranklist(Model model,
                                          Principal principal,
                                          @PathVariable int beer_id) {
        User user = userService.getByUsername(principal.getName());
        userService.removeFromList("dranklist", user.getId(), beer_id);

        model.addAttribute("beers",
                userService.showList("dranklist", user.getId()));
        return "show-dranklist";
    }

    @RequestMapping("/rate/{beer_id}")
    public String rateBeer(Model model,
                            String rating,
                           Principal principal,
                           @PathVariable int beer_id) {
        int rate = Integer.parseInt(rating);
        User user = userService.getByUsername(principal.getName());
        userService.rateBeer(user.getId(), beer_id, rate);

        model.addAttribute("beers",
                userService.showList("dranklist", user.getId()));
        return "show-dranklist";
    }
}
