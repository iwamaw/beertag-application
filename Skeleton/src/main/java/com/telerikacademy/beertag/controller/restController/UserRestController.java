package com.telerikacademy.beertag.controller.restController;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserService service;


    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    @PostMapping
    public User create(@RequestBody @Valid User newUser) {
        try {
            service.create(newUser);
            return newUser;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public User update(@RequestBody @Valid User user) {
        try {
            service.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/rate/{user_id}/{beer_id}/{rating}")
    public void rateBeer(@PathVariable int user_id,@PathVariable int beer_id, @PathVariable int rating){
        try{
            service.rateBeer(user_id, beer_id, rating);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PutMapping("/{user_id}/{list}/{beer_id}")
    public void addToList(@PathVariable int user_id, @PathVariable String list, @PathVariable int beer_id) {
        try {
            service.addToList(list, user_id, beer_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        } catch (IllegalArgumentException i) {
            throw new IllegalArgumentException(i.getMessage());
        }catch (DuplicateEntityException d){
            throw new ResponseStatusException(HttpStatus.CONFLICT,d.getMessage());
        }
    }

    @PutMapping("/delete/{user_id}/{list}/{beer_id}")
    public void removeFromList(@PathVariable int user_id, @PathVariable String list, @PathVariable int beer_id) {
        try {
            service.removeFromList(list, user_id, beer_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        } catch (IllegalArgumentException i) {
            throw new IllegalArgumentException(i.getMessage());
        }
    }

    @GetMapping("/{user_id}/{list}")
    public Set<Beer> showListOfBeers(@PathVariable int user_id,@PathVariable String list){
        try {
            return service.showList(list, user_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException i) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, i.getMessage());
        }
    }


    @DeleteMapping
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{username}")
    public User getByUsername(@PathVariable String username) {
        try {
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
