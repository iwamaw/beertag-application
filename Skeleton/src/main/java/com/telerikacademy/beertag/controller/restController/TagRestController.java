package com.telerikacademy.beertag.controller.restController;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import org.springframework.web.server.ResponseStatusException;
import com.telerikacademy.beertag.services.common.TagService;
import org.springframework.web.bind.annotation.*;
import com.telerikacademy.beertag.models.Tag;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {

    private TagService service;

    public TagRestController(TagService service) {
        this.service = service;
    }

    @GetMapping
    public List<Tag> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Tag getOne(@PathVariable int id) {
        try {
            return service.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@RequestBody @Valid Tag tag) {
        try {
            service.create(tag);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
