package com.telerikacademy.beertag.controller;


import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.server.ResponseStatusException;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.telerikacademy.beertag.models.UserDTO;
import org.springframework.stereotype.Controller;
import com.telerikacademy.beertag.models.User;
import org.springframework.http.HttpStatus;
import com.cloudinary.utils.ObjectUtils;
import org.cloudinary.json.JSONObject;
import org.springframework.ui.Model;
import com.cloudinary.Cloudinary;

import javax.validation.Valid;

import java.security.Principal;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.io.File;


@Controller
public class RegistrationController {

    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    @Qualifier("com.cloudinary.cloud_name")
    String mCloudName;

    @Autowired
    @Qualifier("com.cloudinary.api_key")
    String mApiKey;

    @Autowired
    @Qualifier("com.cloudinary.api_secret")
    String mApiSecret;


    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder,
                                  UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping("/register_page")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDTO());
        return "register_page";
    }

    @GetMapping("/edit-user/{username}")
    public String showEditUserPage(Model model,
                                   @PathVariable String username){
        User user = userService.getByUsername(username);
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(user.getUsername());

        model.addAttribute("user",new UserDTO());
        return "edit-user";
    }


    @PostMapping("/register_page")
    public String registerUser(Model model,
                               @Valid @ModelAttribute UserDTO user,
                               BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", new UserDTO());
            model.addAttribute("error", "Username/password can't be empty!");
            return "register_page";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("user", new UserDTO());
            model.addAttribute("error", "User with the same username already exists!");
            return "register_page";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("user", new UserDTO());

            model.addAttribute("error", "Password does't match!");

            return "register_page";
        }


        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);

        return "login_page";
    }


    @PostMapping("/edit-user/{username}")
    public String editUser(@PathVariable("username") String username,
                           @ModelAttribute ("user") UserDTO user,
                           @RequestParam MultipartFile aFile,
                           Principal principal) {
        User updatedUser = userService.getByUsername(principal.getName());

        updatedUser.setEmail(user.getEmail());
        updatedUser.setFirstName(user.getFirstName());
        updatedUser.setLastName(user.getLastName());
        updatedUser.setProfilePicUrl(getPictureURL(aFile));

        try {
            userService.update(updatedUser);
            return "redirect:/";
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    private String getPictureURL(MultipartFile aFile) {
        Cloudinary c = new Cloudinary("cloudinary://" + mApiKey + ":" + mApiSecret + "@" + mCloudName);
        String url = "";
        try {
            File f = Files.createTempFile("temp", aFile.getOriginalFilename()).toFile();
            aFile.transferTo(f);

            Map response = c.uploader().upload(f, ObjectUtils.emptyMap());
            JSONObject json = new JSONObject(response);
            url = json.getString("url");
            url = url.substring(0, 49) + "w_500,h_500,c_fill/" + url.substring(49);

        } catch (Exception e) {
            throw new EntityNotFoundException("Picture not found!");
        }
        return url;
    }

    @GetMapping("/register-success")
    public String showRegisterConfirmation() {
        return "/register-confirmation";
    }
}
