package com.telerikacademy.beertag.controller.restController;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.services.common.StyleService;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;
import com.telerikacademy.beertag.models.Style;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

    private StyleService service;

    public StyleRestController(StyleService service) {
        this.service = service;
    }

    @GetMapping
    public List<Style> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Style getOne(@PathVariable int id) {
        try {
            return service.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@RequestBody @Valid Style style) {
        try {
            service.create(style);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
