package com.telerikacademy.beertag.controller;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.multipart.MultipartFile;
import com.telerikacademy.beertag.services.common.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.telerikacademy.beertag.models.*;
import org.springframework.http.HttpStatus;
import com.cloudinary.utils.ObjectUtils;
import org.cloudinary.json.JSONObject;
import org.springframework.ui.Model;
import com.cloudinary.Cloudinary;

import java.security.Principal;
import javax.validation.Valid;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.util.Map;

@Controller
public class BeerController {

    private BreweryService breweryService;
    private StyleService styleService;
    private BeerService beerService;
    private UserService userService;
    private TagService tagService;

    @Autowired
    @Qualifier("com.cloudinary.cloud_name")
    String mCloudName;

    @Autowired
    @Qualifier("com.cloudinary.api_key")
    String mApiKey;

    @Autowired
    @Qualifier("com.cloudinary.api_secret")
    String mApiSecret;


    @Autowired
    public BeerController(BreweryService breweryService,
                          StyleService styleService,
                          BeerService beerService,
                          UserService userService,
                          TagService tagService) {
        this.breweryService = breweryService;
        this.styleService = styleService;
        this.beerService = beerService;
        this.userService = userService;
        this.tagService = tagService;
    }

    @GetMapping("/menu")
    public String showBeers(Model model) {
        model.addAttribute("beers", beerService.getAll());
        return "menu";
    }

    @GetMapping("/beer/{id}")
    public String getOneBeer(Model model,
                             @PathVariable int id) {
        try {
            model.addAttribute("beer", beerService.getOne(id));
            return "beerProfile";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @RequestMapping("/add/wishlist/{beer_id}")
    public String addBeerToWishList(Model model,
                                    @PathVariable int beer_id, Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            userService.addToList("wishlist", user.getId(), beer_id);
            model.addAttribute("beer", beerService.getOne(beer_id));
            return "beerProfile";
        } catch (DuplicateEntityException d) {
            return "error-page-beers-menus";
        }
    }

    @RequestMapping("/add/dranklist/{beer_id}")
    public String addBeerToDrankList(Model model, @PathVariable int beer_id, Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            userService.addToList("dranklist", user.getId(), beer_id);
            model.addAttribute("beer", beerService.getOne(beer_id));
        } catch (DuplicateEntityException d) {
            return "error-page-beers-menus";
        }
        return "beerProfile";
    }

    @RequestMapping("/search/style")
    public String filterByStyle(String style, Model model) {
        model.addAttribute("beers", beerService.filterByStyle(style));
        return "filterByStyle";
    }

    @GetMapping("/filter/style")
    public String showBeersInStyleFilter(Model model) {
        model.addAttribute("beers", beerService.getAll());
        return "filterByStyle";
    }

    @GetMapping("/filter/country")
    public String showBeersInCountryFilter(Model model) {
        model.addAttribute("beers", beerService.getAll());
        return "filterByCountry";
    }

    @RequestMapping("/search/country")
    public String filterByCountry(Model model,
                                  String country) {
        model.addAttribute("beers", beerService.filterByCountry(country));
        return "filterByCountry";
    }


    @GetMapping("/sort/abv")
    public String sortByABV(Model model) {
        model.addAttribute("beers", beerService.sortByABV());
        return "sortedByABV";
    }

    @GetMapping("sort/name")
    public String sortByName(Model model) {
        model.addAttribute("beers", beerService.sortByName());
        return "sortedByName";
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("breweries", breweryService.getAll());
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("tags", tagService.getAll());
        model.addAttribute("beer", new BeerDTO());
        return "beer";
    }

    @PostMapping("/beers/new")
    public String createBeer(Model model,
                             @Valid @ModelAttribute("beer") BeerDTO beer,
                             @RequestParam MultipartFile aFile) {

        Beer newBeer = mapperCreate(beer);
        newBeer.setBeerPicture(getPictureURL(aFile));
        beerService.create(newBeer);
        model.addAttribute("beers", beerService.getAll());
        return "menu";
    }

    @GetMapping("/beer/name")
    public String getByName(Model model,
                            @ModelAttribute("name") String name) {
        model.addAttribute("name", beerService.getByName(name));
        return "beers";
    }

    @RequestMapping("/search")
    public String getBeers(String beerName, Model model) {
        model.addAttribute("beers", beerService.getByName(beerName));
        return "menu";
    }

    private Beer mapperCreate(BeerDTO beerDTO) {
        try {
            Beer newBeer = new Beer();
            return mainMapper(newBeer, beerDTO);
            //TODO tags
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND);
        }
    }

    private Beer mainMapper(Beer newBeer, BeerDTO beerDTO) {
        try {
            newBeer.setName(beerDTO.getName());
            newBeer.setDescription(beerDTO.getDescription());
            newBeer.setAbv(beerDTO.getAbv());

            Style style = styleService.getOne(beerDTO.getStyleID());
            newBeer.setStyle(style);

            Brewery brewery = breweryService.getOne(beerDTO.getBreweryID());
            newBeer.setBrewery(brewery);

            Tag tag = tagService.getOne(beerDTO.getTagID());
            List<Tag> tags = new ArrayList<>();
            tags.add(tag);
            newBeer.setTags(tags);

            return newBeer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/edit-beer/{id}")
    public String getEditBeer(@PathVariable int id, Model model) {
        Beer beer = beerService.getOne(id);

        BeerDTO beerDTO = new BeerDTO();
        beerDTO.setName(beer.getName());
        beerDTO.setDescription(beer.getDescription());
        beerDTO.setAbv(beer.getAbv());
        beerDTO.setBreweryID(beer.getBrewery().getID());
        beerDTO.setStyleID(beer.getStyle().getID());

        model.addAttribute("style", styleService.getAll());
        model.addAttribute("beerDTO", beerDTO);
        model.addAttribute("beer", beer);

        return "edit-beer";
    }

    @PostMapping("/edit-beer/{id}")
    public String putEditBeer(@PathVariable("id") int id,
                              @Valid @ModelAttribute("beer") Beer beer,
                              BindingResult errors,
                              @RequestParam MultipartFile aFile) {
        Beer toUpdate = beerService.getOne(id);

        toUpdate.setName(beer.getName());
        toUpdate.setDescription(beer.getDescription());
        toUpdate.setAbv(beer.getAbv());
        try {
            toUpdate.setBeerPicture(getPictureURL(aFile));
        } catch (Exception ignored) {

        }

        beerService.update(toUpdate);
        return "redirect:/";
    }

    private String getPictureURL(MultipartFile aFile) {
        Cloudinary c = new Cloudinary("cloudinary://" + mApiKey + ":" + mApiSecret + "@" + mCloudName);
        String url = "";
        try {
            File f = Files.createTempFile("temp", aFile.getOriginalFilename()).toFile();
            aFile.transferTo(f);

            Map response = c.uploader().upload(f, ObjectUtils.emptyMap());
            JSONObject json = new JSONObject(response);
            url = json.getString("url");
            url = url.substring(0, 49) + "w_500,h_500,c_fill/" + url.substring(49);

        } catch (Exception e) {
            throw new EntityNotFoundException("Picture not found!");
        }
        return url;
    }

}
