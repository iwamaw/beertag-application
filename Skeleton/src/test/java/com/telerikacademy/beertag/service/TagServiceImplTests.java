package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.TagRepository;
import com.telerikacademy.beertag.services.TagServiceImpl;
import com.telerikacademy.beertag.models.Tag;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createTag;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository repository;

    @InjectMocks
    TagServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenTagExists() {

        List<Tag> expected = new ArrayList<>();
        expected.add(createTag());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Tag returned = mockService.getOne(0);

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnTag_whenTagExist() {

        List<Tag> expected = new ArrayList<>();
        expected.add(createTag());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Tag returned = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_shouldEntityStatusException_whenTagNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Tag tag = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getAll_Should_CallRepository() {

        List<Tag> expected = new ArrayList<>();
        expected.add(createTag());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<Tag> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenTagExists() {

        List<Tag> list = new ArrayList<>();
        Tag tag = createTag();
        list.add(tag);
        when(repository.getOne(anyInt())).thenReturn(list);

        mockService.delete(tag.getID());

        Mockito.verify(repository, times(1)).delete(any(Tag.class));
    }

    @Test
    public void create_Should_CallRepository_WhenCreatingStyle() {

        Tag expected = createTag();
        doNothing().when(repository).create(any(Tag.class));

        mockService.create(expected);

        Mockito.verify(repository, Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_Should_ThrowDuplicateException_WhenStyleExists() {

        Tag expected = createTag();
        Mockito.when(repository.checkTagExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository, times(1)).getOne(anyInt());

    }
}
