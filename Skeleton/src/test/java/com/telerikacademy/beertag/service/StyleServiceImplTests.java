package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.StyleRepository;
import com.telerikacademy.beertag.services.StyleServiceImpl;
import com.telerikacademy.beertag.models.Style;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createStyle;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository repository;

    @InjectMocks
    StyleServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenStyleExists() {

        List<Style> expected = new ArrayList<>();
        expected.add(createStyle());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Style returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnStyle_whenStyleExist() {

        List<Style> expected = new ArrayList<>();
        expected.add(createStyle());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Style returned = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_shouldEntityStatusException_whenStyleNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Style style = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getAll_Should_CallRepository() {

        List<Style> expected = new ArrayList<>();
        expected.add(createStyle());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<Style> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenStyleExists() {

        List<Style> list = new ArrayList<>();
        Style style = createStyle();
        list.add(style);
        when(repository.getOne(anyInt())).thenReturn(list);

        mockService.delete(style.getID());

        Mockito.verify(repository, times(1)).delete(any(Style.class));
    }

    @Test
    public void create_Should_CallRepository_WhenCreatingStyle() {

        Style expected = createStyle();
        doNothing().when(repository).create(any(Style.class));

        mockService.create(expected);

        Mockito.verify(repository, Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_Should_ThrowDuplicateException_WhenStyleExists() {

        Style expected = createStyle();
        Mockito.when(repository.checkStyleExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository, times(1)).getOne(anyInt());

    }
}
