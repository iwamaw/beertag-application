package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.UsersRepository;
import com.telerikacademy.beertag.services.UserServiceImpl;
import com.telerikacademy.beertag.models.User;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createUser;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UsersRepository repository;

    @InjectMocks
    UserServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenUserExists() {

        List<User> expected = new ArrayList<>();
        expected.add(createUser());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        User returnedUser = mockService.getOne(0);

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnUser_whenUserExist() {

        List<User> expected = new ArrayList<>();
        expected.add(createUser());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        User returned = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_shouldEntityStatusException_whenUserNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        User user = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getByUsername_Should_CallRepository_WhenUsernameExist() {

        User expected = createUser();
        List<User> list = new ArrayList<>();
        list.add(expected);
        Mockito.when(repository.getByUsername(anyString()))
                .thenReturn(list);

        User returnUsername = mockService.getByUsername("");

        Mockito.verify(repository, Mockito.times(1)).getByUsername(anyString());
    }

    @Test
    public void getByUsername_shouldReturnUsername_whenUserExist() {

        List<User> expected = new ArrayList<>();
        expected.add(createUser());
        Mockito.when(repository.getByUsername(anyString()))
                .thenReturn(expected);

        User returned = mockService.getByUsername(anyString());

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByUsername_shouldEntityStatusException_whenUserNotExist() {

        Mockito.when(repository.getByUsername(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        User user = mockService.getByUsername(anyString());

        Mockito.verify(repository, times(1)).getByUsername(anyString());
    }


    @Test
    public void createUser_Should_CallRepository_WhenCreatingUser() {

        User expected = createUser();
        doNothing().when(repository).create(any(User.class));

        mockService.create(expected);

        Mockito.verify(repository, Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_Should_ThrowDuplicateException_WhenUserExists() {

        User expected = createUser();
        Mockito.when(repository.checkUserExists(expected.getUsername())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository, times(1)).getOne(anyInt());

    }

    @Test
    public void getAll_Should_CallRepository() {

        List<User> expected = new ArrayList<>();
        expected.add(createUser());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<User> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenUserExists() {

        User user = createUser();
        List<User> list = new ArrayList<>();
        list.add(user);
        when(repository.getOne(anyInt())).thenReturn(list);

        mockService.delete(user.getId());

        Mockito.verify(repository, times(1)).delete(any(User.class));
    }

    @Test
    public void update_Should_CallRepository_WhenUserExists() {

        User expected = createUser();
        Mockito.when(repository.checkUserExists(expected.getUsername())).thenReturn(true);
        doNothing().when(repository).update(any(User.class));

        mockService.update(expected);

        Mockito.verify(repository, times(1)).update(any(User.class));
    }

    @Test
    public void update_Should_ReturnNewUser_WhenOldUserDoesExists() {

        User expected = createUser();
        Mockito.when(repository.checkUserExists(expected.getUsername())).thenReturn(true);

        mockService.update(expected);

        Mockito.verify(repository, times(1)).update(any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_ThrowEntityNotFoundException_WhenUserDoesNotExists() {

        User user = createUser();
        Mockito.when(repository.checkUserExists(user.getUsername()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.update(user);

        Mockito.verify(repository, times(1)).update(user);
    }


}
