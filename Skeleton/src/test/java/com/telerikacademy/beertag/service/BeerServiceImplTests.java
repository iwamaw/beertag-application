package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.services.BeerServiceImpl;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.models.Beer;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createBeer;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository repository;

    @InjectMocks
    BeerServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenBeerExists() {

        List<Beer> expected = new ArrayList<>();
        expected.add(createBeer());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Beer returnedBeer = mockService.getOne(anyInt());

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnBeer_whenBeerExist() {

        List<Beer> expected = new ArrayList<>();
        expected.add(createBeer());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Beer returnedBeer = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returnedBeer);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_shouldThrowEntityException_whenBeerNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Beer beer = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void createBeer_Should_CallRepository_WhenCreatingBeer() {

        Beer expected = createBeer();
        doNothing().when(repository).create(any(Beer.class));

        mockService.create(expected);

        Mockito.verify(repository, Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBeer_Should_ThrowDuplicateException_WhenBeerExists() {

        Beer expected = createBeer();

        Mockito.when(repository.checkBeerExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository, times(1)).getOne(anyInt());

    }

    @Test
    public void getAll_Should_CallRepository() {

        List<Beer> expected = new ArrayList<>();
        expected.add(createBeer());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<Beer> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenBeerExists() {
        List<Beer> list = new ArrayList<>();
        Beer beer = createBeer();
        list.add(beer);
        when(repository.getOne(anyInt())).thenReturn(list);

        mockService.delete(beer.getID());

        Mockito.verify(repository, times(1)).delete(any(Beer.class));
    }


    @Test
    public void update_Should_CallRepository_WhenBeerExists() {

        Beer expected = createBeer();
        Mockito.when(repository.checkBeerExists(expected.getName())).thenReturn(true);
        doNothing().when(repository).update(any(Beer.class));

        mockService.update(expected);

        Mockito.verify(repository, times(1)).update(any(Beer.class));
    }

    @Test
    public void update_Should_ReturnNewBeer_WhenOldBeerDoesExists() {

        Beer expected = createBeer();
        Mockito.when(repository.checkBeerExists(expected.getName())).thenReturn(true);

        mockService.update(expected);

        Mockito.verify(repository, times(1)).update(any(Beer.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_ThrowEntityNotFoundException_WhenBeerDoesNotExists() {

        Beer beer = createBeer();
        Mockito.when(repository.checkBeerExists(beer.getName()))
                .thenThrow(new EntityNotFoundException(""));

        mockService.update(beer);

        Mockito.verify(repository, times(1)).update(beer);
    }

    @Test
    public void getByName_Should_CallRepository() {

        Beer beer = createBeer();

        mockService.getByName(beer.getName());

        Mockito.verify(repository, times(1)).getByName(beer.getName());
    }

    @Test
    public void filterByStyle_Should_CallRepository() {
        Style style = new Style("test");

        mockService.filterByStyle(style.getName());

        Mockito.verify(repository, times(1)).filterByStyle(style.getName());

    }

    @Test
    public void filterByCountry_Should_CallRepository() {
        Country country = new Country("test");

        mockService.filterByCountry(country.getName());

        Mockito.verify(repository, times(1)).filterByCountry(country.getName());
    }

    @Test
    public void filterByTag_Should_CallRepository() {
        mockService.filterByTag(anyInt());
        Mockito.verify(repository, times(1)).filterByTag(anyInt());

    }

    @Test
    public void sortByABV_Should_CallRepository() {
        mockService.sortByABV();
        Mockito.verify(repository, times(1)).sortByABV();
    }

    @Test
    public void sortByName_Should_CallRepository() {
        mockService.sortByName();
        Mockito.verify(repository, times(1)).sortByName();
    }

    @Test
    public void sortByRating_Should_CallRepository() {
        mockService.sortByRating();
        Mockito.verify(repository, times(1)).sortByRating();
    }

}
