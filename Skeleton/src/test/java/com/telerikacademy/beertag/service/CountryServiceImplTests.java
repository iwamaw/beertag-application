package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.repository.common.CountryRepository;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.services.CountryServiceImpl;
import com.telerikacademy.beertag.models.Country;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.junit.Assert;
import org.mockito.Mock;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createCountry;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository repository;

    @InjectMocks
    CountryServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenCountryExists() {

        List<Country> expected = new ArrayList<>();
        expected.add(createCountry());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Country returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnCountry_whenCountryExist() {

        List<Country> expected = new ArrayList<>();
        expected.add(createCountry());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Country returned = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_Should_ThrowEntityStatusException_whenCountryDoesNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Country country = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }


    @Test
    public void getAll_Should_CallRepository() {

        List<Country> expected = new ArrayList<>();
        expected.add(createCountry());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<Country> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }

}
