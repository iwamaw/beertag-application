package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.repository.common.BreweryRepository;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.services.BreweryServiceImpl;
import com.telerikacademy.beertag.models.Brewery;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.junit.Assert;
import org.mockito.Mock;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)

public class BreweryServiceTests {

    @Mock
    BreweryRepository repository;

    @InjectMocks
    BreweryServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenBreweryExists() {
        List<Brewery> expected = new ArrayList<>();
        expected.add(createBrewery());

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Brewery returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnCountry_whenBreweryExist() {

        List<Brewery> expected = new ArrayList<>();
        expected.add(createBrewery());
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Brewery returned = mockService.getOne(1);

        Assert.assertSame(expected.get(0), returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_Should_ThrowEntityStatusException_whenBreweryDoesNotExist() {

        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Brewery brewery = mockService.getOne(anyInt());

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }


    @Test
    public void getAll_Should_CallRepository() {

        List<Brewery> expected = new ArrayList<>();
        expected.add(createBrewery());
        Mockito.when(repository.getAll()).thenReturn(expected);

        List<Brewery> returned = mockService.getAll();

        Mockito.verify(repository, times(1)).getAll();
    }


    @Test
    public void createBrewery_Should_CallRepository_WhenCreatingBrewery() {

        Brewery expected = createBrewery();
        doNothing().when(repository).create(any(Brewery.class));

        mockService.create(expected);

        Mockito.verify(repository, Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBrewery_Should_ThrowDuplicateException_WhenBreweryExists() {

        Brewery expected = createBrewery();
        Mockito.when(repository.checkBreweryExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository, times(1)).getOne(anyInt());

    }

    @Test
    public void delete_Should_CallRepository_WhenBeerExists() {

        List<Brewery> list = new ArrayList<>();
        Brewery brewery = createBrewery();
        list.add(brewery);
        when(repository.getOne(anyInt())).thenReturn(list);

        mockService.delete(brewery.getID());

        Mockito.verify(repository, times(1)).delete(any(Brewery.class));
    }

}
