package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.repository.common.RatingRepository;
import com.telerikacademy.beertag.services.RatingServiceImpl;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Mock;
import org.junit.Test;

import static com.telerikacademy.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)

public class RatingServiceTests {

    @Mock
    RatingRepository repository;

    @InjectMocks
    RatingServiceImpl mockService;


    @Test
    public void createRating_Should_CallRepository_WhenCreatingRating() {
        //Arrange

        User user = createUser();
        Beer beer = createBeer();

        doNothing().when(repository).create(any(User.class), any(Beer.class), anyInt());

        //Act
        mockService.create(user, beer, 3);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).create(any(User.class), any(Beer.class), anyInt());
    }

}
