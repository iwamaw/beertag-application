package com.telerikacademy.beertag;

import com.telerikacademy.beertag.models.*;

public class Factory {

    public static Beer createBeer(){
        return new Beer("test");
    }

    public static Country createCountry(){
        return new Country("test");
    }

    public static Brewery createBrewery(){return new Brewery("test");}

    public static Rating createRating(){
        return new Rating(new User(),createBeer(),4);
    }

    public static User createUser(){
        return new User();
    }

    public static Style createStyle(){
        return new Style("test");
    }

    public static Tag createTag() {
        return new Tag("test");
    }
}
